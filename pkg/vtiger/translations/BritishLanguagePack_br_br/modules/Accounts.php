<?php
/**
 * Copyright (C) 2006-2014 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yucheng.hu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-5.4.0 分支，适用于 vTiger 5.4.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'Accounts'                     => '客户'               , 
	'SINGLE_Accounts'              => '客户'                , 
	'LBL_ADD_RECORD'               => '添加客户'            , 
	'LBL_RECORDS_LIST'             => '客戶列表'           , 
	'LBL_ACCOUNT_INFORMATION'      => '客戶信息'    , 
	'LBL_SHOW_ACCOUNT_HIERARCHY'   => 'Show Organisation Hierarchy' , 
	'industry'                     => '行业'                    , 
	'Account Name'                 => '组织名称'           , 
	'Account No'                   => '组织号. '            , 
	'Website'                      => '网站'                     , 
	'Ticker Symbol'                => '股票代码'               , 
	'Member Of'                    => '上级公司'                   , 
	'Employees'                    => '雇员'                   , 
	'Ownership'                    => '所有者'                   , 
	'SIC Code'                     => '标准产业分类代码'                    , 
	'Other Email'                  => '其他电子邮件'                 , 
	'Analyst'                      => '待分析（Analyst）'                     , 
	'Competitor'                   => '竞争对手'                  , 
	'Customer'                     => '客户'                    , 
	'Integrator'                   => '集成商'                  , 
	'Investor'                     => '投资人'                    , 
	'Press'                        => 'Press'                       , 
	'Prospect'                     => '前景展望（Prospect）'                    , 
	'Reseller'                     => '经销商'                    , 
	'LBL_START_DATE'               => '开始日期'                  , 
	'LBL_END_DATE'                 => '结束日期'                    , 
	'LBL_DUPLICATES_EXIST'         => '客戶名已存在', 
	'LBL_COPY_SHIPPING_ADDRESS'    => 'Copy Shipping Address'       , // TODO: Review
	'LBL_COPY_BILLING_ADDRESS'     => 'Copy Billing Address'        , // TODO: Review
);
$jsLanguageStrings = array(
	'LBL_RELATED_RECORD_DELETE_CONFIRMATION' => 'Are you sure you want to Delete?', 
	'LBL_DELETE_CONFIRMATION'      => 'Deleting this Organization will remove its related Opportunities & Quotes. Are you sure you want to delete this Organization?', 
	'LBL_MASS_DELETE_CONFIRMATION' => 'Deleting this Organization will remove its related Opportunities & Quotes. Are you sure you want to delete this Organization?', 
);