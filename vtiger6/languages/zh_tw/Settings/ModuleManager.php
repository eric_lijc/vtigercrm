<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'LBL_IMPORT_MODULE'            => 'Import Module'               , // TODO: Review
	'LBL_VTIGER_EXTENSION_STORE'   => 'Vtiger Extension Store'      , // TODO: Review
	'LBL_PUBLISHER'                => 'Publisher'                   , // TODO: Review
	'LBL_LICENSE'                  => 'License'                     , // TODO: Review
	'LBL_PUBLISHED_ON'             => 'Published on'                , // TODO: Review
	'LBL_INSTALL'                  => 'Install'                     , // TODO: Review
	'LBL_UPGRADE'                  => 'Upgrade'                     , // TODO: Review
	'LBL_VERSION'                  => 'Version'                     , // TODO: Review
	'LBL_DECLINE'                  => 'Decline'                     , // TODO: Review
	'LBL_ACCEPT_AND_INSTALL'       => 'Accept and Install'          , // TODO: Review
	'LBL_ALREADY_EXISTS'           => 'Already Exists'              , // TODO: Review
	'LBL_OK'                       => 'OK'                          , // TODO: Review
	'LBL_EXTENSION_NOT_COMPATABLE' => 'Extension is not Vtiger Compatable', // TODO: Review
	'LBL_INVALID_FILE'             => 'Invalid File'                , // TODO: Review
	'LBL_NO_LICENSE_PROVIDED'      => 'No License Provided'         , // TODO: Review
	'LBL_INSTALLATION'             => 'Installation'                , // TODO: Review
	'LBL_FAILED'                   => 'Failed'                      , // TODO: Review
	'LBL_SUCCESSFULL'              => 'Successfull'                 , // TODO: Review
	'LBL_INSTALLATION_LOG'         => 'Installation Log'            , // TODO: Review
);