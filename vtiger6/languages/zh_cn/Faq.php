<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'Faq' => 'FAQ',
	'SINGLE_Faq' => 'FAQ',
	'LBL_RECORDS_LIST' => 'FAQs List',
	'LBL_ADD_RECORD' => 'Add FAQ',
	
	//Blocks
	'LBL_FAQ_INFORMATION' => 'FAQ Information',
	'LBL_COMMENT_INFORMATION' =>  'Comments',
	
	//Fields
	'Question'=>'Question',
	'Answer'=>'Answer',
	'Comments'=>'Comments',
	'Faq No' => 'Faq Number',
	
	//Added for existing Picklist Entries
	'General'=>'General',
	'Draft'=>'Draft',
	'Published'=>'Published',
	'Obsolete'=>'Obsolete',

	//EditView
	'LBL_SOLUTION' => 'Solution',
);