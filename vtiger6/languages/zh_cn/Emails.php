<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'SINGLE_Emails' => 'Email',
	'Emails' => 'Emails',
	'LBL_SELECT_EMAIL_IDS' => 'Select Email Addresses',
	'LBL_SUBJECT' => 'Subject',
	'LBL_ATTACHMENT' => 'Attachment',
	'LBL_BROWSE_CRM' => 'Browse CRM',
	'LBL_SEND' => 'Send',
	'LBL_SAVE_AS_DRAFT' => 'Save as Draft',
	'LBL_GO_TO_PREVIEW' => 'Go to Preview',
	'LBL_SELECT_EMAIL_TEMPLATE' => 'Select Email Template',
	'LBL_COMPOSE_EMAIL' => 'Compose Email',
	'LBL_TO' => 'To',
 	'LBL_CC' => 'Cc',
   	'LBL_BCC' => 'Bcc',
   	'LBL_ADD_CC' => 'Add Cc',
   	'LBL_ADD_BCC' => 'Add Bcc',
	'LBL_MAX_UPLOAD_SIZE' => 'Maximum upload size is',
	'LBL_EXCEEDED' => 'Exceeded',
	
	//Button Names translation
	'LBL_FORWARD' => 'Forward',
	'LBL_PRINT' => 'Print',
	'LBL_DESCRIPTION' => 'Description',
	'LBL_FROM' => 'From',
	'LBL_INFO' => 'Info',
	'LBL_DRAFTED_ON' => 'Drafted on',
	'LBL_SENT_ON' => 'Sent on',
	'LBL_OWNER' => 'Owner',
	
	'Date & Time Sent' => 'Date Sent',
);
