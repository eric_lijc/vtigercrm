<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Accounts' => 'Organizations',
	'SINGLE_Accounts' => 'Organization',
	'LBL_ADD_RECORD' => 'Add Organization',
	'LBL_RECORDS_LIST' => 'Organization List',

	// Blocks
	'LBL_ACCOUNT_INFORMATION' => 'Organization Details',

	// Mass Action
	'LBL_SHOW_ACCOUNT_HIERARCHY' => 'Organization Hierarchy',

	//Field Labels
	'industry' => 'Industry',
	'Account Name' => 'Organization Name',
	'Account No' => 'Organization Number',
	'Website' => 'Website',
	'Ticker Symbol' => 'Ticker Symbol',
	'Member Of' => 'Member Of',
	'Employees' => 'Employees',
	'Ownership' => 'Ownership',
	'SIC Code' => 'SIC Code',
	'Other Email' => 'Secondary Email',
	
	//Added for existing picklist entries

	'Analyst'=>'Analyst',
	'Competitor'=>'Competitor',
	'Customer'=>'Customer',
	'Integrator'=>'Integrator',
	'Investor'=>'Investor',
	'Press'=>'Press',
	'Prospect'=>'Prospect',
	'Reseller'=>'Reseller',
	'LBL_START_DATE' => 'Start Date',
	'LBL_END_DATE' => 'End Date',
	
	//Duplication error message
	'LBL_DUPLICATES_EXIST' => 'Organization Name already exists',
	'LBL_COPY_SHIPPING_ADDRESS' => 'Copy Shipping Address',
	'LBL_COPY_BILLING_ADDRESS' => 'Copy Billing Address',
);

$jsLanguageStrings = array(
	'LBL_RELATED_RECORD_DELETE_CONFIRMATION' => 'Are you sure you want to delete?',
	'LBL_DELETE_CONFIRMATION' => 'Deleting this Organization will remove its related Opportunities & Quotes. Are you sure you want to delete this Organization?',
	'LBL_MASS_DELETE_CONFIRMATION' => 'Deleting this Organization(s) will remove its related Opportunities & Quotes. Are you sure you want to delete the selected records?',
	'JS_DUPLICTAE_CREATION_CONFIRMATION' => 'Organization Name already Exists.Do you want to create a duplicate record?'
);