<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'SINGLE_Settings:Webforms'     => 'Webform'                     , // TODO: Review
	'WebForm Name'                 => 'Webform Name'                , // TODO: Review
	'Public Id'                    => 'Public Id'                   , // TODO: Review
	'Enabled'                      => 'Status'                      , // TODO: Review
	'Module'                       => 'Module'                      , // TODO: Review
	'Return Url'                   => 'Return Url'                  , // TODO: Review
	'Post Url'                     => 'Post Url'                    , // TODO: Review
	'SINGLE_Webforms'              => 'Webform'                     , // TODO: Review
	'LBL_SHOW_FORM'                => 'Show Form'                   , // TODO: Review
	'LBL_DUPLICATES_EXIST'         => 'Webform Name already exists' , // TODO: Review
	'LBL_WEBFORM_INFORMATION'      => 'Webform Information'         , // TODO: Review
	'LBL_FIELD_INFORMATION'        => 'Field Information'           , // TODO: Review
	'LBL_FIELD_NAME'               => 'Field Name'                  , // TODO: Review
	'LBL_OVERRIDE_VALUE'           => 'Override Value'              , // TODO: Review
	'LBL_MANDATORY'                => 'Mandatory'                   , // TODO: Review
	'LBL_WEBFORM_REFERENCE_FIELD'  => 'Webforms reference Field'    , // TODO: Review
	'LBL_SELECT_FIELDS_OF_TARGET_MODULE' => 'Select Fields for Target Module...', // TODO: Review
	'LBL_ALLOWS_YOU_TO_MANAGE_WEBFORMS' => 'Allows you to manage webforms', // TODO: Review
	'LBL_ADD_FIELDS'               => 'Add Fields'                  , // TODO: Review
	'LBL_EMBED_THE_FOLLOWING_FORM_IN_YOUR_WEBSITE' => 'Embed the following form in your website', // TODO: Review
	'LBL_SELECT_VALUE'             => 'Select Value'                , // TODO: Review
	'LBL_LABEL'                    => 'label'                       , // TODO: Review
);
$jsLanguageStrings = array(
	'JS_WEBFORM_DELETED_SUCCESSFULLY' => 'Webform deleted successfully', // TODO: Review
	'JS_LOADING_TARGET_MODULE_FIELDS' => 'Loadding Target Module Fields', // TODO: Review
	'JS_SELECT_VALUE'              => 'Select Vlaue'                , // TODO: Review
);