<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */


/* NOTE: Should be inline with Calendar language translation but few variations */

$languageStrings = array(
	// Basic Strings
	'Events' => '事件',
	'SINGLE_Events' => '事件',
	'LBL_ADD_RECORD' => '添加事件',
	'LBL_RECORDS_LIST' => '事件列表',
	'LBL_EVENTS' => 'Events',
	'LBL_TODOS' => 'To Do',
	'LBL_HOLD_FOLLOWUP_ON' => 'Hold Followup on',
	
	// Blocks
	'LBL_EVENT_INFORMATION' => 'Event Details',
	'LBL_RECURRENCE_INFORMATION' => 'Recurrence Details',
    'LBL_RELATED_TO' => 'Related To',
	
	//Fields
	'Start Date & Time'=>'Start Date & Time',
	'Recurrence' => 'Recurrence',
	'Send Notification' => 'Send Notification',
	'Location'=>'Location',
	'Send Reminder' => 'Send Reminder',
	'End Date & Time' => 'End Date & Time',
	'Activity Type'=>'Activity Type',
	'Visibility' => 'Visibility',
	'Recurrence' => 'Repeat',
	
	//Visibility picklist values
	'Private' => 'Private',
	'Public' => 'Public',
	
	//Activity Type picklist values
	'Call' => 'Call',
	'Meeting' => 'Meeting',
	
	//Status picklist values
	'Planned' => 'Planned',
	'Held' => 'Held',
	'Not Held' => 'Not Held',
	
	//Reminder Labels
	'LBL_DAYS' => 'Days',
	'LBL_HOURS' => 'Hours',
	
	//Repeat Labels
	'LBL_DAYS_TYPE' => 'Day(s)',
	'LBL_WEEKS_TYPE' => 'Week(s)',
	'LBL_MONTHS_TYPE' => 'Month(s)',
	'LBL_YEAR_TYPE' => 'Year',
	
	'LBL_FIRST' => 'First',
	'LBL_LAST' => 'Last',
	
	'LBL_SM_SUN' => 'Sun',
	'LBL_SM_MON' => 'Mon',
	'LBL_SM_TUE' => 'Tue',
	'LBL_SM_WED' => 'Wed',
	'LBL_SM_THU' => 'Thr',
	'LBL_SM_FRI' => 'Fri',
	'LBL_SM_SAT' => 'Sat',
	
	'LBL_DAY0' => 'Sunday',
	'LBL_DAY1' => 'Monday',
	'LBL_DAY2' => 'Tuesday',
	'LBL_DAY3' => 'Wednesday',
	'LBL_DAY4' => 'Thursday',
	'LBL_DAY5' => 'Friday',
	'LBL_DAY6' => 'Saturday',
	
	'Daily'=>'Day(s)',
	'Weekly'=>'Week(s)',
	'Monthly'=>'Month(s)',
	'Yearly'=>'Year',
	
	'LBL_REPEATEVENT' => 'Repeat once in every',
	'LBL_UNTIL' => 'Until',
	'LBL_DAY_OF_THE_MONTH' => 'day of the month',
	'LBL_ON' => 'on',
	
	'LBL_RECORDS_LIST' => 'List View',
	'LBL_CALENDAR_VIEW' => 'Calendar View',

    'LBL_INVITE_USER_BLOCK' => 'Invite',
    'LBL_INVITE_USERS' => 'Invite Users'

);