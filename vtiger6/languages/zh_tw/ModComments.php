<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_ModComments' => 'Comment',
	'LBL_RECORDS_LIST' => 'Comments List',

	// Blocks
	'LBL_MODCOMMENTS_INFORMATION' => 'Comments',
	'LBL_OTHER_INFORMATION' => 'Other Information',

	'LBL_ADDING_COMMENT' => 'Adding Comment',
	'LBL_WRITE_YOUR_COMMENT_HERE' => 'Enter comments here',
	
	//Field Labels
	'Comment' => 'Comment',
	'Creator' => 'Creator',
	'Related To Comments' => 'Related To',
	
);