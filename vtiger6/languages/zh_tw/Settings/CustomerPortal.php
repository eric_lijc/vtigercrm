<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'CustomerPortal'               => 'Customer Portal'             , // TODO: Review
	'LBL_PORTAL_DESCRIPTION'       => 'Setup Privileges of Portal User', // TODO: Review
	'LBL_PRIVILEGES'               => 'Privileges'                  , // TODO: Review
	'LBL_DEFAULT_ASSIGNEE'         => 'Default Assignee'            , // TODO: Review
	'LBL_PORTAL_URL'               => 'Portal Url'                  , // TODO: Review
	'LBL_MODULE_NAME'              => 'Module Name'                 , // TODO: Review
	'LBL_ENABLE_MODULE'            => 'Enable Module'               , // TODO: Review
	'LBL_VIEW_ALL_RECORDS'         => 'See Records across Organization', // TODO: Review
	'LBL_PREVILEGES_MESSAGE'       => 'This Role\'s privileges will be applied to the Portal User.', // TODO: Review
	'LBL_DEFAULT_ASSIGNEE_MESSAGE' => 'Tickets will be Assigned to the selected Assignee by the default Group/User from the Customer Portal.', // TODO: Review
	'LBL_PORTAL_URL_MESSAGE'       => 'This is URL for the Portal where your contacts can login to submit/track tickets, access knowledge base and do more. Contacts will be sent the login details when Portal access is enabled from Contact details page.', // TODO: Review
	'LBL_DRAG_AND_DROP_MESSAGE'    => 'Drag and Drop modules to reorder in the Customer Portal', // TODO: Review
);
$jsLanguageStrings = array(
	'JS_PORTAL_INFO_SAVED'         => 'Customer Portal settings saved', // TODO: Review
);