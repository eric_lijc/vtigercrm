<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Vendors' => 'Vendors',
	'SINGLE_Vendors' => 'Vendor',
	'LBL_ADD_RECORD' => 'Add Vendor',
	'LBL_RECORDS_LIST' => 'Vendors List',

	// Blocks
	'LBL_VENDOR_INFORMATION' => 'Vendor Details',
	'LBL_VENDOR_ADDRESS_INFORMATION' => 'Address Details',
	
	//Field Labels
	'Vendor Name' => 'Vendor Name',
	'Vendor No' => 'Vendor Number',
	'Website' => 'Website',
	'GL Account' => 'GL Account',
	
	//Added for existing Picklist entries

	'300-Sales-Software'=>'300-Sales-Software',
	'301-Sales-Hardware'=>'301-Sales-Hardware',
	'302-Rental-Income'=>'302-Rental-Income',
	'303-Interest-Income'=>'303-Interest-Income',
	'304-Sales-Software-Support'=>'304-Sales-Software-Support',
	'305-Sales Other'=>'305-Sales Other',
	'306-Internet Sales'=>'306-Internet Sales',
	'307-Service-Hardware Labor'=>'307-Service-Hardware Labor',
	'308-Sales-Books'=>'308-Sales-Books',
);

$jsLanguageStrings = array(
	'LBL_RELATED_RECORD_DELETE_CONFIRMATION' => 'Are you sure you want to delete?',
	'LBL_DELETE_CONFIRMATION' => 'Deleting this Vendor will remove its related PurchaseOrders. Are you sure you want to delete this Vendor?',
	'LBL_MASS_DELETE_CONFIRMATION' => 'Deleting this vendor(s) will remove its related Purchase Orders. Are you sure you want to delete the selected Records?',
);