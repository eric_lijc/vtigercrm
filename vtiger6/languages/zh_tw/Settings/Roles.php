<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'Roles'                        => 'Roles'                       , 
	'SINGLE_Roles'                 => 'Role'                        , 
	'LBL_ADD_RECORD'               => 'Add Role'                    , 
	'LBL_DELETE_ROLE'              => 'Delete Role'                 , // TODO: Review
	'LBL_TRANSFER_OWNERSHIP'       => 'Transfer Ownership'          , // TODO: Review
	'LBL_TO_OTHER_ROLE'            => 'To other Role'               , // TODO: Review
	'LBL_CLICK_TO_EDIT_OR_DRAG_TO_MOVE' => 'Click to edit/Drag to move'  , // TODO: Review
	'LBL_ASSIGN_ROLE'              => 'Assign Role'                 , // TODO: Review
	'LBL_CHOOSE_PROFILES'          => 'Choose profiles'             , // TODO: Review
	'LBL_COPY_PRIVILEGES_FROM'     => 'Copy privileges from'        , // TODO: Review
	'LBL_PROFILE'                  => 'Profile'                     , // TODO: Review
	'LBL_REPORTS_TO'               => 'Reports To'                  , // TODO: Review
	'LBL_NAME'                     => 'Name'                        , // TODO: Review
	'LBL_ASSIGN_NEW_PRIVILEGES'    => 'Assign privileges directly to Role', // TODO: Review
	'LBL_ASSIGN_EXISTING_PRIVILEGES' => 'Assign priviliges from existing profiles', // TODO: Review
	'LBL_PRIVILEGES'               => 'Privileges'                  , // TODO: Review
);