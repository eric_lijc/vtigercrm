<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	
	'Asterisk' => 'Asterisk',
	'PBXManager' => 'PBX 管理',
	'SINGLE_PBXManager' => 'PBX 管理',
	
	//Blocks
	'LBL_CALL_INFORMATION' => 'Call Details',
	
	//Field Labels
	'Call From'=>'Call From',
	'Call To'=>'Call To',
	'Time Of Call'=>'Time Of Call',
	'PBXManager ID'=>'PBX Manager ID',
);