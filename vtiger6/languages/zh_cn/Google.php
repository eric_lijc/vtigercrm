<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

    $languageStrings = array(
        'Map' => 'Map',
        'EXTENTIONNAME' => 'Google',
        'LBL_UPDATES_CRM' => 'Updates in CRM',
        'LBL_UPDATES_GOOGLE' => 'Updates in Google',
        'LBL_UPDATED' => 'Updated',
        'LBL_ADDED' => 'Added',
        'LBL_DELETED' => 'Deleted',
        'LBL_SYNCRONIZED' => 'Syncronized',
        'LBL_NOT_SYNCRONIZED' => 'You have not syncronized yet',
        'LBL_SYNC_BUTTON' => 'Syncronize Now',
        'LBL_MORE_VTIGER' => 'There are more records to be syncronized in vtiger',
        'LBL_MORE_GOOGLE' => 'There are more records to be syncronized in Google',
        'Contact Name'=>'Contact Name',
        'Email'=>'Email',
        'Mobile Phone'=>'Mobile Phone',
        'Address' => 'Address',
        'Event Title'=>'Event Title',
        'Start Date'=>'Start Date',
        'Until Date'=>'Until Date',
        'Description'=>'Description'
    );
    $jsLanguageStrings = array(
        'LBL_SYNC_BUTTON' => 'Syncronize Now',
        'LBL_SYNCRONIZING' => 'Syncronizing....',
        'LBL_NOT_SYNCRONIZE' => 'You have not syncronized yet'
    );