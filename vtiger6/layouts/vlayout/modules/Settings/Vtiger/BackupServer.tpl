{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ********************************************************************************/
-->*}
{strip}
	<div class="row-fluid">
		<span class="widget_header row-fluid">
			<div class="row-fluid"><h3>{vtranslate('LBL_BACKUP_SERVER_SETTINGS', $QUALIFIED_MODULE)}</h3></div>
		</span>
	</div>
	<hr>
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span12">
				<h2>Backup Server  View</h2>
				<p>Generic implementation of the Backup Server page view...</p>
			</div>
		</div>
	</div>
{/strip}