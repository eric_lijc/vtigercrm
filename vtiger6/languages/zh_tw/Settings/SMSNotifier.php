<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'LBL_ADD_RECORD'               => 'New Configuration'           , // TODO: Review
	'SMSNotifier'                  => 'SMS Provider Configuration'  , // TODO: Review
	'LBL_ADD_CONFIGURATION'        => 'New Configuration'           , // TODO: Review
	'LBL_EDIT_CONFIGURATION'       => 'Edit Configuration'          , // TODO: Review
	'LBL_SELECT_ONE'               => 'Select One'                  , // TODO: Review
	'providertype'                 => 'Provider'                    , // TODO: Review
	'isactive'                     => 'Active'                      , // TODO: Review
	'username'                     => 'User Name'                   , // TODO: Review
	'password'                     => 'Password'                    , // TODO: Review
);
$jsLanguageStrings = array(
	'LBL_DELETE_CONFIRMATION'      => 'Are you sure, you want to delete this SMSNotifier Configuration', // TODO: Review
	'JS_RECORD_DELETED_SUCCESSFULLY' => 'SMS Provider Deleted Successfully', // TODO: Review
	'JS_CONFIGURATION_SAVED'       => 'SMS Provider Configurations saved', // TODO: Review
);