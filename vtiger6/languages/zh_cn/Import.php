<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'LBL_IMPORT_STEP_1' => 'Step 1',
	'LBL_IMPORT_STEP_1_DESCRIPTION' => 'Select File',
	'LBL_IMPORT_SUPPORTED_FILE_TYPES' => '	Supported File Type(s): .CSV, .VCF',
	'LBL_IMPORT_STEP_2' => 'Step 2',
	'LBL_IMPORT_STEP_2_DESCRIPTION' => 'Specify Format',
	'LBL_FILE_TYPE' => 'File Type',
	'LBL_CHARACTER_ENCODING' => 'Character Encoding',
	'LBL_DELIMITER' => 'Delimiter',
	'LBL_HAS_HEADER' => 'Has Header',
	'LBL_IMPORT_STEP_3' => 'Step 3',
	'LBL_IMPORT_STEP_3_DESCRIPTION' => 'Duplicate Record Handling',
	'LBL_IMPORT_STEP_3_DESCRIPTION_DETAILED' => 'Select this option to enable and set duplicate merge criteria',
	'LBL_SPECIFY_MERGE_TYPE' => 'Select how duplicate records should be handled',
	'LBL_SELECT_MERGE_FIELDS' => 'Select the matching fields to find duplicate records',
	'LBL_AVAILABLE_FIELDS' => 'Available Fields',
	'LBL_SELECTED_FIELDS' => 'Fields to be matched on',
	'LBL_NEXT_BUTTON_LABEL' => 'Next',
	'LBL_IMPORT_STEP_4' => 'Step 4',
	'LBL_IMPORT_STEP_4_DESCRIPTION' => 'Map the Columns to Module Fields',
	'LBL_FILE_COLUMN_HEADER' => 'Header',
	'LBL_ROW_1' => 'Row 1',
	'LBL_CRM_FIELDS' => 'CRM Fields',
	'LBL_DEFAULT_VALUE' => 'Default Value',
	'LBL_SAVE_AS_CUSTOM_MAPPING' => 'Save as Custom Mapping ',
	'LBL_IMPORT_BUTTON_LABEL' => 'Import',
	'LBL_RESULT' => 'Result',
	'LBL_TOTAL_RECORDS_IMPORTED' => 'Records successfully imported',
	'LBL_NUMBER_OF_RECORDS_CREATED' => 'Records created',
	'LBL_NUMBER_OF_RECORDS_UPDATED' => 'Records overwritten',
	'LBL_NUMBER_OF_RECORDS_SKIPPED' => 'Records skipped',
	'LBL_NUMBER_OF_RECORDS_MERGED' => 'Records merged', 
	'LBL_TOTAL_RECORDS_FAILED' => 'Records failed importing',
	'LBL_IMPORT_MORE' => 'Import More',
	'LBL_VIEW_LAST_IMPORTED_RECORDS' => 'Last Imported Records',
	'LBL_UNDO_LAST_IMPORT' => 'Undo Last Import',
	'LBL_FINISH_BUTTON_LABEL' => 'Finish',
	'LBL_UNDO_RESULT' => 'Undo Import Result',
	'LBL_TOTAL_RECORDS' => 'Total Number of Records',
	'LBL_NUMBER_OF_RECORDS_DELETED' => 'Number of records deleted',
	'LBL_OK_BUTTON_LABEL' => 'Ok',
	'LBL_IMPORT_SCHEDULED' => 'Import Scheduled',
	'LBL_RUNNING' => 'Running',
	'LBL_CANCEL_IMPORT' => 'Cancel Import',
	'LBL_ERROR' => 'Error',
	'LBL_CLEAR_DATA' => 'Clear Data',
	'ERR_UNIMPORTED_RECORDS_EXIST' => 'Unable to import more data in this batch. Please start a new import.',
	'ERR_IMPORT_INTERRUPTED' => 'Current Import has been interrupted. Please try again later',
	'ERR_FAILED_TO_LOCK_MODULE' => 'Failed to lock the module for import. Re-try again later',
	'LBL_SELECT_SAVED_MAPPING' => 'Select Saved Mapping',
	'LBL_IMPORT_ERROR_LARGE_FILE' => 'Import Error Large file ',
	'LBL_FILE_UPLOAD_FAILED' => 'File Upload Failed',
	'LBL_IMPORT_CHANGE_UPLOAD_SIZE' => 'Import Change Upload Size',
	'LBL_IMPORT_DIRECTORY_NOT_WRITABLE' => 'Import Directory is not writable',
	'LBL_IMPORT_FILE_COPY_FAILED' => 'Import File copy failed',
	'LBL_INVALID_FILE' => 'Invalid File',
	'LBL_NO_ROWS_FOUND' => 'No rows found',
	'LBL_SCHEDULED_IMPORT_DETAILS' => 'Your import has been scheduled and will start within 15 minutes. You will receive an email after import is completed.  <br> <br>
										Please make sure that the Outgoing server and your email address is configured to receive email notification',
	'LBL_DETAILS' => 'Details',
	'skipped' => 'Skipped Records',
	'failed' => 'Failed Records',
	
);
