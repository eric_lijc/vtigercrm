<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'HelpDesk' => 'Tickets',
	'SINGLE_HelpDesk' => 'Ticket',
	'LBL_ADD_RECORD' => 'Add Ticket',
	'LBL_RECORDS_LIST' => 'Ticket List',

	// Blocks
	'LBL_TICKET_INFORMATION' => 'Ticket Information',
	'LBL_TICKET_RESOLUTION' => 'Ticket Resolution',

	//Field Labels
	'Ticket No' => 'Ticket Number',
	'Severity' => 'Severity',
	'Update History' => 'Update History',
	'Hours' => 'Hours',
	'Days' => 'Days',
	'Title' => 'Title',
	'Solution' => 'Solution',
	'From Portal' => 'From Portal',

	//Added for existing picklist entries

	'Big Problem'=>'Big Problem',
	'Small Problem'=>'Small Problem',
	'Other Problem'=>'Other Problem',

	'Normal'=>'Normal',
	'High'=>'High',
	'Urgent'=>'Urgent',

	'Minor'=>'Minor',
	'Major'=>'Major',
	'Feature'=>'Feature',
	'Critical'=>'Critical',

	'Open'=>'Open',
	'Wait For Response'=>'Wait For Response',
	'Closed'=>'Closed',
	'LBL_STATUS' => 'Status',
	'LBL_SEVERITY' => 'Severity',
	//DetailView Actions
	'LBL_CONVERT_FAQ' => 'Convert to FAQ',
	'LBL_RELATED_TO' => 'Related To'
);