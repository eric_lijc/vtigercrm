<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_Assets' => 'Asset',
	'LBL_ADD_RECORD' => 'Add Asset',
	'LBL_RECORDS_LIST' => 'Assets List',

	// Blocks
	'LBL_ASSET_INFORMATION' => 'Asset Details',

	//Field Labels
    'Asset No' => 'Asset No',
	'Serial Number' => 'Serial Number',
	'Date Sold' => 'Date Sold',
	'Date in Service' => 'Date in Service',
	'Tag Number' => 'Tag Number',
	'Invoice Name' => 'Invoice Name',
	'Shipping Method' => 'Shipping Method',
	'Shipping Tracking Number' => 'Shipping Tracking Number',
	'Asset Name' => 'Asset Name',
	'Customer Name' => 'Customer Name',
	'Notes' => 'Notes',

	/*picklist values*/
	'In Service'=>'In Service',
	'Out-of-service'=>'Out-of-service',
);