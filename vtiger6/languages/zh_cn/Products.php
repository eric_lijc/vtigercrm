<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Products' => 'Products',
	'SINGLE_Products' => 'Product',
	'LBL_ADD_RECORD' => 'Add Product',
	'LBL_RECORDS_LIST' => 'Products List',

	// Blocks
	'LBL_PRODUCT_INFORMATION' => 'Product Details',
	'LBL_IMAGE_INFORMATION' => 'Product Image Information',
	'LBL_STOCK_INFORMATION' => 'Stock Information',
	
	'LBL_MORE_CURRENCIES' => 'more currencies', 
	'LBL_PRICES' => 'Product Prices',
	'LBL_PRICE' => 'Price',
	'LBL_RESET_PRICE' => 'Reset Price',
	'LBL_RESET' => 'Reset',
	'LBL_ADD_TO_PRICEBOOKS' => 'Add to PriceBooks',

	//Field Labels
	'Product No' => 'Product Number',
	'Part Number' => 'Part Number',
	'Product Active' => 'Product Active',
	'Manufacturer' => 'Manufacturer',
	'Product Category' => 'Product Category',
	'Website' => 'Website',
	'Mfr PartNo' => 'Mfr PartNo',
	'Vendor PartNo' => 'Vendor PartNo',
	'Usage Unit'=>'Usage Unit',
	'Handler'=>'Handler',
	'Reorder Level'=>'Reorder Level',
	'Tax Class'=>'Tax Class',
	'Reorder Level'=>'Reorder Level',
	'Vendor PartNo'=>'Vendor Part No',
	'Serial No'=>'Serial No',
	'Qty In Stock'=>'Qty. in Stock',
	'Product Sheet'=>'Product Sheet',
	'Qty In Demand'=>'Qty. in Demand',
	'GL Account'=>'GL Account',
	'Product Image'=>'Product Image',
	'Unit Price'=>'Unit Price',
	'Commission Rate'=>'Commission Rate',
	'Qty/Unit'=>'Qty/Unit',
	
	//Added for existing picklist entries

	'--None--'=>'--None--',

	'Hardware'=>'Hardware',
	'Software'=>'Software',
	'CRM Applications'=>'CRM Applications',

	'300-Sales-Software'=>'300-Sales-Software',
	'301-Sales-Hardware'=>'301-Sales-Hardware',
	'302-Rental-Income'=>'302-Rental-Income',
	'303-Interest-Income'=>'303-Interest-Income',
	'304-Sales-Software-Support'=>'304-Sales-Software-Support',
	'305-Sales Other'=>'305-Sales Other',
	'306-Internet Sales'=>'306-Internet Sales',
	'307-Service-Hardware Labor'=>'307-Service-Hardware Labor',
	'308-Sales-Books'=>'308-Sales-Books',

	'Box'=>'Box',
	'Carton'=>'Carton',
	'Caton'=>'Caton',
	'Dozen'=>'Dozen',
	'Each'=>'Each',
	'Hours'=>'Hours',
	'Impressions'=>'Impressions',
	'Lb'=>'Lb',
	'M'=>'M',
	'Pack'=>'Pack',
	'Pages'=>'Pages',
	'Pieces'=>'Pieces',
	'Reams'=>'Reams',
	'Sheet'=>'Sheet',
	'Spiral Binder'=>'Spiral Binder',
	'Sq Ft'=>'Sq Ft',
	
	'LBL_ADD_TO_PRICEBOOKS' => 'Add to PriceBooks',
	'LBL_CONVERSION_RATE' => 'Conversion Rate',
);