<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Campaigns' => 'Campaigns',
	'SINGLE_Campaigns' => 'Campaign',
	'LBL_ADD_RECORD' => 'Add Campaign',
	'LBL_RECORDS_LIST' => 'Campaigns List',

	// Blocks
	'LBL_CAMPAIGN_INFORMATION' => 'Campaign Details',
	'LBL_EXPECTATIONS_AND_ACTUALS' => 'Expectations & Actuals',
	
	//Field Labels
	'Campaign Name' => 'Campaign Name',
	'Campaign No' => 'Campaign No', 
	'Campaign Type' => 'Campaign Type', 
	'Product' => 'Product',
	'Campaign Status' => 'Campaign Status',
	'Num Sent' => 'Num Sent',
	'Sponsor' => 'Sponsor',
	'Target Audience' => 'Target Audience',
	'TargetSize' => 'TargetSize',
	'Expected Response' => 'Expected Response',
	'Expected Revenue' => 'Expected Revenue',
	'Budget Cost' => 'Budget Cost',
	'Actual Cost' => 'Actual Cost',
	'Expected Response Count' => 'Expected Response Count',
	'Expected Sales Count' => 'Expected Sales Count',
	'Expected ROI' => 'Expected ROI',
	'Actual Response Count' => 'Actual Response Count',
	'Actual Sales Count' => 'Actual Sales Count',
	'Actual ROI' => 'Actual ROI',
	
	//Added for existing Picklist Entries

	'Webinar'=>'Webinar',
	'Referral Program'=>'Referral Program',
	'Advertisement'=>'Advertisement',
	'Banner Ads'=>'Banner Ads',
	'Direct Mail'=>'Direct Mail',
	'Telemarketing'=>'Telemarketing',
	'Others'=>'Others',

	'Planning'=>'Planning',						      	    
	'Inactive'=>'Inactive',
	'Complete'=>'Complete',
	'Cancelled'=>'Cancelled',							      

	'Excellent'=>'Excellent',
	'Good'=>'Good',
	'Average'=>'Average',
	'Poor'=>'Poor',
	
	// status fields 
	'--None--'=>'--None--',
	'Contacted - Successful' => 'Contacted - Successful',
	'Contacted - Unsuccessful' => 'Contacted - Unsuccessful',
	'Contacted - Never Contact Again' => 'Contacted - Never Contact Again',
);