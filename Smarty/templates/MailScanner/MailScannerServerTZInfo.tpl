{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
  * ("License"); You may not use this file except in compliance with the License
  * The Original Code is:  vtiger CRM Open Source
  * The Initial Developer of the Original Code is vtiger.
  * Portions created by vtiger are Copyright (C) vtiger.
  * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

{if $SELECTEDTZ == "-12:00"}(GMT -12:00 hours) Eniwetok, Kwajalein
{elseif $SELECTEDTZ == "-11:00"}(GMT -11:00 hours) Midway Island, Samoa
{elseif $SELECTEDTZ == "-10:00"}(GMT -10:00 hours) Hawaii
{elseif $SELECTEDTZ == "-9:00"}(GMT -9:00 hours) Alaska
{elseif $SELECTEDTZ == "-8:00"}(GMT -8:00 hours) Pacific Time (US & Canada)
{elseif $SELECTEDTZ == "-7:00"}(GMT -7:00 hours) Mountain Time (US & Canada)
{elseif $SELECTEDTZ == "-6:00"}(GMT -6:00 hours) Central Time (US & Canada), Mexico City
{elseif $SELECTEDTZ == "-5:00"}(GMT -5:00 hours) Eastern Time (US & Canada), Bogota, Lima, Quito
{elseif $SELECTEDTZ == "-4:00"}(GMT -4:00 hours) Atlantic Time (Canada), Caracas, La Paz
{elseif $SELECTEDTZ == "-3:30"}(GMT -3:30 hours) Newfoundland
{elseif $SELECTEDTZ == "-3:00"}(GMT -3:00 hours) Brazil, Buenos Aires, Georgetown
{elseif $SELECTEDTZ == "-2:00"}(GMT -2:00 hours) Mid-Atlantic
{elseif $SELECTEDTZ == "-1:00"}(GMT -1:00 hours) Azores, Cape Verde Islands
{elseif $SELECTEDTZ == "0:00"}(GMT) Western Europe Time, London, Lisbon, Casablanca, Monrovia
{elseif $SELECTEDTZ == "+1:00"}(GMT +1:00 hours) CET(Central Europe Time), Brussels, Copenhagen, Madrid, Paris
{elseif $SELECTEDTZ == "+2:00"}(GMT +2:00 hours) EET(Eastern Europe Time), Kaliningrad, South Africa
{elseif $SELECTEDTZ == "+3:00"}(GMT +3:00 hours) Baghdad, Kuwait, Riyadh, Moscow, St. Petersburg, Volgograd, Nairobi
{elseif $SELECTEDTZ == "+3:30"}(GMT +3:30 hours) Tehran
{elseif $SELECTEDTZ == "+4:00"}(GMT +4:00 hours) Abu Dhabi, Muscat, Baku, Tbilisi
{elseif $SELECTEDTZ == "+4:30"}(GMT +4:30 hours) Kabul
{elseif $SELECTEDTZ == "+5:00"}(GMT +5:00 hours) Ekaterinburg, Islamabad, Karachi, Tashkent
{elseif $SELECTEDTZ == "+5:30"}(GMT +5:30 hours) Bombay, Calcutta, Madras, New Delhi
{elseif $SELECTEDTZ == "+6:00"}(GMT +6:00 hours) Almaty, Dhaka, Colombo
{elseif $SELECTEDTZ == "+7:00"}(GMT +7:00 hours) Bangkok, Hanoi, Jakarta
{elseif $SELECTEDTZ == "+8:00"}(GMT +8:00 hours) Beijing, Perth, Singapore, Hong Kong, Chongqing, Urumqi, Taipei
{elseif $SELECTEDTZ == "+9:00"}(GMT +9:00 hours) Tokyo, Seoul, Osaka, Sapporo, Yakutsk
{elseif $SELECTEDTZ == "+9:30"}(GMT +9:30 hours) Adelaide, Darwin
{elseif $SELECTEDTZ == "+10:00"}(GMT +10:00 hours) EAST(East Australian Standard), Guam, Papua New Guinea, Vladivostok
{elseif $SELECTEDTZ == "+11:00"}(GMT +11:00 hours) Magadan, Solomon Islands, New Caledonia
{elseif $SELECTEDTZ == "+12:00"}(GMT +12:00 hours) Auckland, Wellington, Fiji, Kamchatka, Marshall Island
{/if}