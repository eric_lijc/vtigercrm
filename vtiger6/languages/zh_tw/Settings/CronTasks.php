<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'CronTasks'                    => 'Scheduler'                   , // TODO: Review
	'Id'                           => 'Id'                          , // TODO: Review
	'Cron Job'                     => 'Cron Job'                    , // TODO: Review
	'Frequency'                    => 'Frequency'                   , // TODO: Review
	'Status'                       => 'Status'                      , // TODO: Review
	'Last Start'                   => 'Last scan started'           , // TODO: Review
	'Last End'                     => 'Last scan ended'             , // TODO: Review
	'Sequence'                     => 'Sequence'                    , // TODO: Review
	'LBL_COMPLETED'                => 'Completed'                   , // TODO: Review
	'LBL_RUNNING'                  => 'Running'                     , // TODO: Review
	'LBL_ACTIVE'                   => 'Active'                      , // TODO: Review
	'LBL_INACTIVE'                 => 'In Active'                   , // TODO: Review
);