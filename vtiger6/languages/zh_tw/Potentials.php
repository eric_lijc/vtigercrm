<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Potentials' => 'Opportunities',
	'SINGLE_Potentials' => 'Opportunity',
	'LBL_ADD_RECORD' => 'Add Opportunity',
	'LBL_RECORDS_LIST' => 'Opportunities List',

	// Blocks
	'LBL_OPPORTUNITY_INFORMATION' => 'Opportunity Details',
	
	//Field Labels
	'Potential No' => 'Opportunity Number',
	'Amount' => 'Amount',
	'Next Step' => 'Next Step',
	'Sales Stage' => 'Sales Stage',
	'Probability' => 'Probability',
	'Campaign Source' => 'Campaign Source',
	'Forecast Amount' => 'Forecast Amount',
	
	//Dashboard widgets
	'Funnel' => 'Sales Funnel',
	'Potentials by Stage' => 'Opportunities by Stage',
	'Total Revenue' => 'Revenue by Salesperson',
	'Top Potentials' => 'Top Opportunities',
	'Forecast' => 'Sales Forecast',
	
	//Added for Existing Picklist Strings

	'Prospecting'=>'Prospecting',
	'Qualification'=>'Qualification',
	'Needs Analysis'=>'Needs Analysis',
	'Value Proposition'=>'Value Proposition',
	'Id. Decision Makers'=>'Identify Decision Makers',
	'Perception Analysis'=>'Perception Analysis',
	'Proposal/Price Quote'=>'Proposal/Quotation',
	'Negotiation/Review'=>'Negotiation/Review',
	'Closed Won'=>'Closed Won',
	'Closed Lost'=>'Closed Lost',

	'--None--'=>'--None--',
	'Existing Business'=>'Existing Business',
	'New Business'=>'New Business',
	'LBL_EXPECTED_CLOSE_DATE_ON' => 'Expected to close on',
	
	//widgets headers
	'LBL_RELATED_CONTACTS' => 'Related Contacts',
	'LBL_RELATED_PRODUCTS' => 'Related Products',
);