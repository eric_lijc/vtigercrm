<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	//DetailView Actions
	'SINGLE_Quotes' => 'Quote',
	'LBL_EXPORT_TO_PDF' => 'Export to PDF',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Quote',
	'LBL_RECORDS_LIST' => 'Quotes List',

	// Blocks
	'LBL_QUOTE_INFORMATION' => 'Quote Details',
	
	//Field Labels
	'Quote No'=>'Quote Number',
	'Quote Stage'=>'Quote Stage',
	'Valid Till'=>'Valid Until',
	'Inventory Manager'=>'Inventory Manager',
	//Added for existing Picklist Entries

	'Accepted'=>'Accepted',
	'Rejected'=>'Rejected',

);
