<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'ServiceContracts' => 'Service Contracts',
	'SINGLE_ServiceContracts' => 'Service Contract',
	'LBL_ADD_RECORD' => 'Add Service Contract',
	'LBL_RECORDS_LIST' => 'Service Contracts List',
	// Blocks
	'LBL_SERVICE_CONTRACT_INFORMATION' => 'Service Contract Details',
	
	//Field Labels
	'Contract No' => 'Contract Number',
	'Start Date' => 'Start Date',
	'End Date' => 'End Date',
	'Tracking Unit' => 'Tracking Unit',
	'Total Units' => 'Total Units',
	'Used Units' => 'Used Units',
	'Progress'=> 'Progress',
	'Planned Duration' => 'Planned Duration (in Days)',
	'Actual Duration' => 'Actual Duration (in Days)',
);