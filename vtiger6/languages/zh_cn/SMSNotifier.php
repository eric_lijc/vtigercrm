<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'LBL_SEND_SMS_TO_SELECTED_NUMBERS' => 'Send SMS to the selected numbers',
	'LBL_STEP_1' => 'Step 1',
	'LBL_STEP_2' => 'Step 2',
	'LBL_SELECT_THE_PHONE_NUMBER_FIELDS_TO_SEND' => 'Select the phone number fields to send',
	'LBL_TYPE_THE_MESSAGE' => 'Type the message',
	'LBL_WRITE_YOUR_MESSAGE_HERE' => 'write your message here',
	'LBL_ADD_MORE_FIELDS' => 'Add more fields',
	'LBL_SEREVER_CONFIG' => 'Server Configuration',

	//DetailView Actions
	'LBL_CHECK_STATUS' => 'Check Status',
	'message' => 'Message',

	//Blocks
	'LBL_SMSNOTIFIER_INFORMATION' => 'SMS Information',
	'SINGLE_SMSNotifier' => 'SMS Notifier',
);