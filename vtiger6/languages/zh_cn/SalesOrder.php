<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	//DetailView Actions
	'SINGLE_SalesOrder' => 'Sales Order',
	'LBL_EXPORT_TO_PDF' => 'Export to PDF',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Sales Order',
	'LBL_RECORDS_LIST' => 'Sales Order List',

	// Blocks
	'LBL_SO_INFORMATION' => 'Sales Order Details',

	//Field labels
	'SalesOrder No'=>'Sales Order Number',
	'Quote Name'=>'Quote Name',
	'Customer No' => 'Customer No',
	'Requisition No'=>'Requisition No',
	'Tracking Number'=>'Tracking Number',
	'Sales Commission' => 'Sales Commission',
	'Purchase Order'=>'Purchase Order',
	'Vendor Terms'=>'Vendor Terms',
	'Pending'=>'Pending',
	'Enable Recurring' => 'Enable Recurring',
	'Frequency' => 'Frequency',
	'Start Period' => 'Start Period',
	'End Period' => 'End Period',
	'Payment Duration' => 'Payment Duration',
	'Invoice Status' => 'Invoice Status',

	//Added for existing Picklist Entries

	'Sub Total'=>'Sub Total',
	'AutoCreated'=>'Auto Created',
	'Sent'=>'Sent',
	'Credit Invoice'=>'Credit Invoice',
	'Paid'=>'Paid',
);
