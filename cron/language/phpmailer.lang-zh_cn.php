<?php
/**
 * Copyright (C) 2006-2014 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://bug.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com

 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$PHPMAILER_LANG = array();
$PHPMAILER_LANG["provide_address"] = '您必须提供至少一个 ' .
		'收件人的电子邮件地址.';
$PHPMAILER_LANG["mailer_not_supported"] = '邮件不被支持.';
$PHPMAILER_LANG["execute"] = '无法执行: ';
$PHPMAILER_LANG["instantiate"] = '不能实例化的邮件功能.';
$PHPMAILER_LANG["authenticate"] = 'SMTP错误：无法验证.';
$PHPMAILER_LANG["from_failed"] = '以下发送地址失败: ';
$PHPMAILER_LANG["recipients_failed"] = 'SMTP错误：以下 ' .
		'收件人失败: ';
$PHPMAILER_LANG["data_not_accepted"] = 'SMTP错误：数据不被接受.';
$PHPMAILER_LANG["connect_host"] = 'SMTP错误：无法连接到SMTP服务器.';
$PHPMAILER_LANG["file_access"] = '无法接受文件: ';
$PHPMAILER_LANG["file_open"] = '文件错误：无法打开文件: ';
$PHPMAILER_LANG["encoding"] = '未知编码: ';
?>
