<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'Accounts'                     => 'Organizations & Contacts'    , // TODO: Review
	'LBL_ADD_CUSTOM_RULE'          => 'Add Custom Privilege Rule'   , 
	'Read Only'                    => 'R'                           , 
	'Read Write'                   => 'R+W'                         , 
	'LBL_ADD_CUSTOM_RULE_TO'       => 'Add Custom Rule to'          , // TODO: Review
	'LBL_CAN_ACCESSED_BY'          => 'Can be accessed by'          , 
	'LBL_PRIVILEGES'               => 'Privileges'                  , 
	'LBL_SHARING_RULE'             => 'Sharing Rules'               , 
	'LBL_RULE_NO'                  => 'Rule No.'                    , 
	'LBL_MODULE'                   => 'Module'                      , // TODO: Review
	'LBL_ADVANCED_SHARING_RULES'   => 'Advanced Sharing Rules'      , // TODO: Review
	'LBL_WITH_PERMISSIONS'         => 'With Permissions'            , // TODO: Review
	'LBL_APPLY_NEW_SHARING_RULES'  => 'Apply New Sharing Rules'     , // TODO: Review
	'LBL_READ'                     => 'Read'                        , // TODO: Review
	'LBL_READ_WRITE'               => 'Read and Write'              , // TODO: Review
	'LBL_CUSTOM_ACCESS_MESG'       => 'No Custom Access Rules defined', // TODO: Review
	'SINGLE_Groups'                => 'Group'                       , // TODO: Review
	'SINGLE_Roles'                 => 'Role'                        , // TODO: Review
	'SINGLE_RoleAndSubordinates'   => 'RoleAndSubordinate'          , // TODO: Review
);
$jsLanguageStrings = array(
	'JS_CUSTOM_RULE_SAVED_SUCCESSFULLY' => 'Custom Sharing Rule Saved Successfully', // TODO: Review
	'JS_SELECT_ANY_OTHER_ACCESSING_USER' => 'Select any other accessing user', // TODO: Review
	'JS_NEW_SHARING_RULES_APPLIED_SUCCESSFULLY' => 'New Sharing Rules Applied Successfully', // TODO: Review
	'JS_DEPENDENT_PRIVILEGES_SHOULD_CHANGE' => 'Opportunities, Tickets, Quotes, SalesOrder & Invoice Access must be set to Private when the Organization Access is set to Private', // TODO: Review
);