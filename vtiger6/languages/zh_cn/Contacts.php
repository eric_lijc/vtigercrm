<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Contacts' => 'Contacts',
	'SINGLE_Contacts' => 'Contact',
	'LBL_ADD_RECORD' => 'Add Contact',
	'LBL_RECORDS_LIST' => 'Contacts List',

	// Blocks
	'LBL_CONTACT_INFORMATION' => 'Basic Information',
	'LBL_CUSTOMER_PORTAL_INFORMATION' => 'Customer Portal Details',
	'LBL_IMAGE_INFORMATION' => 'Profile Picture',
	'LBL_COPY_OTHER_ADDRESS' => 'Copy Other Address',
	'LBL_COPY_MAILING_ADDRESS' => 'Copy Mailing Address',

	//Field Labels
	'Office Phone' => 'Office Phone',
	'Home Phone' => 'Home Phone',
	'Title' => 'Title',
	'Department' => 'Department',
	'Birthdate' => 'Date of Birth',
	'Reports To' => 'Reports To',
	'Assistant' => 'Assistant',
	'Assistant Phone' => 'Assistant Phone',
	'Do Not Call' => 'Do Not Call',
	'Reference' => 'Reference',
	'Portal User' => 'Portal User',
	'Mailing Street' => 'Mailing Street',
	'Mailing City' => 'Mailing City',
	'Mailing State' => 'Mailing State',
	'Mailing Zip' => 'Mailing Zip',
	'Mailing Country' => 'Mailing Country',
	'Mailing PO Box' => 'Mailing P.O. Box',
	'Other Street' => 'Other Street',
	'Other City' => 'Other City',
	'Other State' => 'Other State',
	'Other Zip' => 'Other Zip',
	'Other Country' => 'Other Country',
	'Other PO Box' => 'Other P.O. Box',
	'Contact Image' => 'Contact Image',
	
	//Added for Picklist Values
	'Mr.'=>'Mr.',
	'Ms.'=>'Ms.',
	'Mrs.'=>'Mrs.',
	'Dr.'=>'Dr.',
	'Prof.'=>'Prof.',

	'User List'=>'User List',
);

$jsLanguageStrings = array(
        'LBL_SYNC_BUTTON' => 'Synchronize Now',
        'LBL_SYNCRONIZING' => 'Synchronizing....',
        'LBL_NOT_SYNCRONIZED' => 'You have not synchronized yet',
        'LBL_FIELD_MAPPING' => 'Field Mapping'
 );