<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	//DetailView Actions
	'SINGLE_PurchaseOrder' => 'Purchase Order',
	'LBL_EXPORT_TO_PDF' => 'Export to PDF',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Purchase Order',
	'LBL_RECORDS_LIST' => 'Purchase Order List',
	'LBL_COPY_SHIPPING_ADDRESS' => 'Copy Shipping Address',
	'LBL_COPY_BILLING_ADDRESS' => 'Copy Billing Address',

	// Blocks
	'LBL_PO_INFORMATION' => 'Purchase Order Details',

	//Field Labels
	'PurchaseOrder No' => 'Purchase Order Number',
	'Requisition No' => 'Requisition Number',
	'Tracking Number' => 'Tracking Number',
	'Sales Commission' => 'Sales Commission',
    'LBL_PAID' => 'Paid',
    'LBL_BALANCE' => 'Balance',

	//Added for existing Picklist Entries

	'Received Shipment'=>'Received Shipment',

);