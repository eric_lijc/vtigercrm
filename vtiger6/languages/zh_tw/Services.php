<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Services' => 'Services',
	'SINGLE_Services' => 'Service',
	'LBL_ADD_RECORD' => 'Add Service',
	'LBL_RECORDS_LIST' => 'Services List',

	// Blocks
	'LBL_SERVICE_INFORMATION' => 'Service Details',
	
	'LBL_MORE_CURRENCIES' => 'more currencies', 
	'LBL_PRICES' => 'Service Prices',
	'LBL_PRICE' => 'Price',
	'LBL_RESET_PRICE' => 'Reset Price',
	'LBL_RESET' => 'Reset',
	
	//Services popup of pricebook
	'LBL_ADD_TO_PRICEBOOKS' => 'Add to PriceBooks',

	//Field Labels
	'Service Name'=>'Service Name',
	'Service Active'=>'Active',
	'Service Category'=>'Category',
	'Service No'=>'Service Number',
	'Owner'=>'Owner',
	'No of Units'=>'Number of Units',
	'Commission Rate'=>'Commission Rate',
	'Price'=>'Price',
	'Usage Unit'=>'Usage Unit',
	'Tax Class'=>'Tax Class',
	'Website'=>'Website',
	
	//Services popup of pricebook
	'LBL_ADD_TO_PRICEBOOKS' => 'Add to PriceBooks',
);