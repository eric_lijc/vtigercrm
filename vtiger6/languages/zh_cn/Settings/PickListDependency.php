<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'LBL_ADD_RECORD'               => 'Add Picklist Dependency'     , // TODO: Review
	'LBL_PICKLIST_DEPENDENCY'      => 'Picklist Dependency'         , // TODO: Review
	'LBL_SELECT_MODULE'            => 'Module'                      , // TODO: Review
	'LBL_SOURCE_FIELD'             => 'Source Field'                , // TODO: Review
	'LBL_TARGET_FIELD'             => 'Target Field'                , // TODO: Review
	'LBL_SELECT_FIELD'             => 'Select field'                , // TODO: Review
	'LBL_CONFIGURE_DEPENDENCY_INFO' => 'Click on the respective cell to change the mapping for picklist values of target field', // TODO: Review
	'LBL_CONFIGURE_DEPENDENCY_HELP_1' => 'Only mapped picklist values of the Source field will be shown below (except for first time)', // TODO: Review
	'LBL_CONFIGURE_DEPENDENCY_HELP_2' => 'If you want to see or change the mapping for the other picklist values of Source field, <br/>
										then you can select the values by clicking on <b>\'Select Source values\'</b> button on the right side', // TODO: Review
	'LBL_CONFIGURE_DEPENDENCY_HELP_3' => 'Selected values of the Target field values, are highlighted as', // TODO: Review
	'LBL_SELECT_SOURCE_VALUES'     => 'Select Source Values'        , // TODO: Review
	'LBL_SELECT_SOURCE_PICKLIST_VALUES' => 'Select Source Picklist Values', // TODO: Review
	'LBL_ERR_CYCLIC_DEPENDENCY'    => 'This dependency setup is not allowed as it ends up in some cyclic dependency', // TODO: Review
);
$jsLanguageStrings = array(
	'JS_LBL_ARE_YOU_SURE_YOU_WANT_TO_DELETE' => 'Are you sure you want to delete this picklist dependency?', // TODO: Review
	'JS_DEPENDENCY_DELETED_SUEESSFULLY' => 'Dependency deleted successfully', // TODO: Review
	'JS_PICKLIST_DEPENDENCY_SAVED' => 'Picklist Dependency Saved'   , // TODO: Review
	'JS_DEPENDENCY_ATLEAST_ONE_VALUE' => 'You need to select atleast one value for', // TODO: Review
	'JS_SOURCE_AND_TARGET_FIELDS_SHOULD_NOT_BE_SAME' => 'Source field and Target field should not be same', // TODO: Review
	'JS_SELECT_SOME_VALUE'         => 'Select some value'           , // TODO: Review
);