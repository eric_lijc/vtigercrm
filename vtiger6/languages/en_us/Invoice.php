<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	//DetailView Actions
	'SINGLE_Invoice' => 'Invoice',
	'LBL_EXPORT_TO_PDF' => 'Export to PDF',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Invoice',
	'LBL_RECORDS_LIST' => 'Invoice List',

	// Blocks
	'LBL_INVOICE_INFORMATION' => 'Invoice Details',

	//Field labels
	'Sales Order' => 'Sales Order',
	'Customer No' => 'Customer No',
	'Invoice Date' => 'Invoice Date',
	'Purchase Order' => 'Purchase Order',
	'Sales Commission' => 'Sales Commission',
	'Invoice No' => 'Invoice No',
	'LBL_RECEIVED' => 'Received',
	'LBL_BALANCE' => 'Balance',
	//Added for existing Picklist Entries

	'Sent'=>'Sent',
	'Credit Invoice'=>'Credit Invoice',
	'Paid'=>'Paid',
	'AutoCreated'=>'AutoCreated',
	'Cancel' => 'Cancel',
);
