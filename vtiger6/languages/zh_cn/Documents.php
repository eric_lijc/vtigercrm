<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_Documents' => 'Document',
	'Documents' => 'Documents',
	'LBL_ADD_RECORD' => 'Add Document',
	'LBL_RECORDS_LIST' => 'Documents List',

	// Blocks
	'LBL_NOTE_INFORMATION' => 'Basic Information',
	'LBL_FILE_INFORMATION' => 'File Details',
	'LBL_DESCRIPTION' => 'Description',

	//Field Labels
	'Title' => 'Title',
	'File Name' => 'File Name',
	'Note' => 'Note',
	'File Type' => 'File Type',
	'File Size' => 'File Size',
	'Download Type' => 'Download Type',
	'Version' => 'Version',
	'Active' => 'Active',
	'Download Count' => 'Download Count',
	'Folder Name' => 'Folder Name',
	'Document No' => 'Document No',
	'Last Modified By' => 'Last Modified By',

	//Folder
	'LBL_FOLDER_HAS_DOCUMENTS' => 'Please move documents from folder before deleting',

	//DetailView Actions
	'LBL_DOWNLOAD_FILE' => 'Download File',
	'LBL_CHECK_FILE_INTEGRITY' => 'Check file integrity',

	//EditView
	'LBL_INTERNAL' => 'Internal',
	'LBL_EXTERNAL' => 'External',
	'LBL_MAX_UPLOAD_SIZE' => 'Maximum upload size',

	//ListView Actions
	'LBL_MOVE' => 'Move',
	'LBL_ADD_FOLDER' => 'Add Folder',
	'LBL_FOLDERS_LIST' => 'Folders List',
	'LBL_FOLDERS' => 'Folders',
	'LBL_DOCUMENTS_MOVED_SUCCESSFULLY' => 'Documents Moved Successfully',
	'LBL_DENIED_DOCUMENTS' => 'Denied Documents',
	'MB' => 'MB',

	'LBL_ADD_NEW_FOLDER' => 'Add New Folder',
	'LBL_FOLDER_NAME' => 'Folder Name',
	'LBL_FOLDER_DESCRIPTION' => 'Folder Description',

	//Check file integrity messages
	'LBL_FILE_AVAILABLE' => 'File is available for download',
	'LBL_FILE_NOT_AVAILABLE' => 'This Document is not available for Download',
);

$jsLanguageStrings = array(
	'JS_EXCEEDS_MAX_UPLOAD_SIZE' => 'Exceeded maximum upload size',
	'JS_NEW_FOLDER' => 'New Folder',
	'JS_MOVE_DOCUMENTS' => 'Move Documents',
	//Move documents confirmation message
	'JS_ARE_YOU_SURE_YOU_WANT_TO_MOVE_DOCUMENTS_TO' => 'Are you sure you want to move the file(s) to',
	'JS_FOLDER' => 'folder',
	'JS_OPERATION_DENIED' => 'Operation Denied',
);