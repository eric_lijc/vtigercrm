<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_Project' => 'Project',
	'LBL_ADD_RECORD' => 'Add Project',
	'LBL_RECORDS_LIST' => 'Projects List',

	// Blocks
	'LBL_PROJECT_INFORMATION' => 'Project Details',

	//Field Labels
	'Project Name' => 'Project Name',
	'Start Date' => 'Start Date',
	'Target End Date' => 'Target End Date',
	'Actual End Date' => 'Actual End Date',
	'Project No' => 'Project Number',
	'Target Budget' => 'Target Budget',
	'Project Url' => 'Project Url',
	'Progress' => 'Progress',

	//Summary Information
	'LBL_TASKS_OPEN' => 'Tasks Open',
	'LBL_TASKS_DUE' => 'Tasks Due',
	'LBL_TASKS_COMPLETED' => 'Tasks Completed',
	'LBL_PEOPLE' => 'People',

	//Related List
	'LBL_CHARTS' => 'Charts',
	'LBL_TASKS_LIST' => 'Tasks List',
	'LBL_MILESTONES' => 'Milestones',
	'LBL_TASKS' => 'Tasks',
	'LBL_STATUS_IS' => 'Status is',
	'LBL_STATUS' => 'Status',
	'LBL_TICKET_PRIORITY' => 'Priority',
	'LBL_MORE' => 'more',
	
	//Summary View Widgets
	'LBL_DOWNLOAD_FILE' => 'Download File',
);