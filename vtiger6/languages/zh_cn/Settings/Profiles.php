<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'Profiles'                     => 'Profiles'                    , 
	'SINGLE_Profiles'              => 'Profile'                     , 
	'LBL_ADD_RECORD'               => 'Add Profile'                 , 
	'LBL_CREATE_PROFILE'           => 'Create Profile'              , // TODO: Review
	'LBL_PROFILE_NAME'             => 'Profile name'                , // TODO: Review
	'LBL_DESCRIPTION'              => 'Description'                 , // TODO: Review
	'LBL_EDIT_PRIVILIGES_FOR_THIS_PROFILE' => 'Edit priviliges for this profile', // TODO: Review
	'LBL_MODULES'                  => 'Modules'                     , // TODO: Review
	'LBL_PROFILE_VIEW'             => 'Profile view'                , // TODO: Review
	'LBL_FIELDS'                   => 'Fields'                      , // TODO: Review
	'LBL_TOOLS'                    => 'Tools'                       , // TODO: Review
	'LBL_FIELD_AND_TOOL_PRVILIGES' => 'Field and Tool Privileges'   , // TODO: Review
	'LBL_EDIT_RECORD'              => 'Edit'                        , // TODO: Review
	'LBL_DUPLICATE_RECORD'         => 'Duplicate'                   , // TODO: Review
	'LBL_DELETE_RECORD'            => 'Delete'                      , // TODO: Review
	'LBL_VIEW_PRVILIGE'            => 'View'                        , 
	'LBL_EDIT_PRVILIGE'            => 'Create/Edit'                 , 
	'LBL_DELETE_PRVILIGE'          => 'Delete'                      , 
	'LBL_INIVISIBLE'               => 'Invisible'                   , // TODO: Review
	'LBL_READ_ONLY'                => 'Read only'                   , // TODO: Review
	'LBL_WRITE'                    => 'Write'                       , // TODO: Review
	'LBL_DELETE_PROFILE'           => 'Delete Profile'              , // TODO: Review
	'LBL_TRANSFER_ROLES_TO_PROFILE' => 'Transfer roles to profile'   , // TODO: Review
	'LBL_PROFILES'                 => 'Profiles'                    , // TODO: Review
	'LBL_CHOOSE_PROFILES'          => 'Choose Profiles'             , // TODO: Review
);
$jsLanguageStrings = array(
	'JS_RECORD_DELETED_SUCCESSFULLY' => 'Profile deleted successfully', // TODO: Review
);