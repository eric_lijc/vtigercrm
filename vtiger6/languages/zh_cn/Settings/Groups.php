<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	'LBL_TRANSFORM_OWNERSHIP'      => 'Transfer ownership'          , 
	'SINGLE_Groups'                => 'Group'                       , // TODO: Review
	'LBL_TO_OTHER_GROUP'           => 'To Other Group '             , 
	'LBL_ADD_RECORD'               => 'Add Group'                   , // TODO: Review
	'LBL_GROUP_NAME'               => 'Group Name'                  , // TODO: Review
	'LBL_GROUP_MEMBERS'            => 'Group Members'               , // TODO: Review
	'LBL_ADD_USERS_ROLES'          => 'Add Users, Roles...'         , // TODO: Review
	'LBL_ROLEANDSUBORDINATE'       => 'Role and Subordinates'       , // TODO: Review
	'RoleAndSubordinates'          => 'Role and Subordinates'       , // TODO: Review
);
$jsLanguageStrings = array(
	'JS_PLEASE_SELECT_ATLEAST_ONE_MEMBER_FOR_A_GROUP' => 'Please select atleast one member for a group', // TODO: Review
	'JS_RECORD_DELETED_SUCCESSFULLY' => 'Group deleted successfully'  , // TODO: Review
);