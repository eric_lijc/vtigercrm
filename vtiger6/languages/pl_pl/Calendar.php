<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	// Basic Strings
	'SINGLE_Calendar' => 'Kalendarz',
	'LBL_ADD_TASK' => 'Dodaj zadanie',
	'LBL_ADD_EVENT' => 'Dodaj Czynność',
	'LBL_RECORDS_LIST' => 'Lista wydarzeń',
	'LBL_RECORD_SUMMARY' => 'Podsumowanie wydarzenia',
	'LBL_EVENTS' => 'Czynności',
	'LBL_TODOS' => 'Zadania',

	// Blocks
	'LBL_TASK_INFORMATION' => 'Szczegóły zadania',

	//Fields
	'Subject' => 'Temat',
	'Start Date & Time' => 'Data rozpoczęcia',
	'Activity Type'=>'Typ czynności',
	'Send Notification'=>'Wyślij powiadomienie',
	'Location'=>'Lokalizacja',
	'End Date & Time' => 'Data zakończenia',

	//Side Bar Names
	'LBL_ACTIVITY_TYPES' => 'Typ czynności',
	'LBL_CONTACTS_SUPPORT_END_DATE' => 'Data zakończenia wsparcia',
	'LBL_CONTACTS_BIRTH_DAY' => 'Data urodzin',

	//Activity Type picklist values
	'Call' => 'Telefon',
	'Meeting' => 'Spotkanie',

	//Status picklist values
	'Planned' => 'Planowane',
	'Completed' => 'Zakończone',
	'Pending Input' => 'W realizacji',
	'Not Started' => 'Nierozpoczęte',
	'Deferred' => 'Przełożone',

	//Priority picklist values
	'Medium' => 'Normalny',

	'LBL_CHANGE_OWNER' => 'Zmień właściciela',

	'LBL_EVENT' => 'Czynność',
	'LBL_TASK' => 'Zadanie',

	'LBL_RECORDS_LIST' => 'Widok listy',
	'LBL_CALENDAR_VIEW' => 'Widok kalendarza'

);

$jsLanguageStrings = array(

	'LBL_ADD_EVENT_TASK' => 'Dodaj zadanie / Czynność'
);