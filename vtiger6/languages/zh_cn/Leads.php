<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'Leads' => 'Leads',
	'SINGLE_Leads' => 'Lead',
	'LBL_RECORDS_LIST' => 'Leads List',
	'LBL_ADD_RECORD' => 'Add Lead',

	// Blocks
	'LBL_LEAD_INFORMATION' => 'Lead Details',

	//Field Labels
	'Lead No' => 'Lead Number',
	'Company' => 'Company',
	'Designation' => 'Designation',
	'Website' => 'Website',
	'Industry' => 'Industry',
	'Lead Status' => 'Lead Status',
	'No Of Employees' => 'Number of Employees',

	//Added for Existing Picklist Entries

	'--None--'=>'--None--',
	'Mr.'=>'Mr.',
	'Ms.'=>'Ms.',
	'Mrs.'=>'Mrs.',
	'Dr.'=>'Dr.',
	'Prof.'=>'Prof.',

	//Lead Status Picklist values
	'Attempted to Contact'=>'Attempted to Contact',
	'Cold'=>'Cold',
	'Contact in Future'=>'Contact in Future',
	'Contacted'=>'Contacted',
	'Hot'=>'Hot',
	'Junk Lead'=>'Junk Lead',
	'Lost Lead'=>'Lost Lead',
	'Not Contacted'=>'Not Contacted',
	'Pre Qualified'=>'Pre Qualified',
	'Qualified'=>'Qualified',
	'Warm'=>'Warm',

	// Mass Action
	'LBL_CONVERT_LEAD' => 'Convert Lead',

	//Convert Lead
	'LBL_TRANSFER_RELATED_RECORD' => 'Transfer related record to',
	'LBL_CONVERT_LEAD_ERROR' => 'You have to enable either Organization or Contact to convert the Lead',
	'LBL_CONVERT_LEAD_ERROR_TITLE' => 'Modules Disabled',
	'CANNOT_CONVERT' => 'Cannot Convert',
	'LBL_FOLLOWING_ARE_POSSIBLE_REASONS' => 'Possible reasons include:',
	'LBL_LEADS_FIELD_MAPPING_INCOMPLETE' => 'Leads Field Mapping is incomplete(Settings > Module Manager > Leads > Leads Field Mapping)',
	'LBL_MANDATORY_FIELDS_ARE_EMPTY' => 'Mandatory fields are empty',
	'LBL_LEADS_FIELD_MAPPING' => 'Leads Field Mapping',

	//Leads Custom Field Mapping
	'LBL_CUSTOM_FIELD_MAPPING'=> 'Edit Field Mapping',
	'LBL_WEBFORMS' => 'Setup Webforms',
	'LBL_LEAD_SOURCE' => 'Lead Source'
);
$jsLanguageStrings = array(
	'JS_SELECT_CONTACTS' => 'Select Contacts to proceed',
	'JS_SELECT_ORGANIZATION' => 'Select Organization to proceed',
	'JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_LEAD' => 'Conversion requires selection of Contact or Organization'
);