<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = Array(
	'RecycleBin' => 'Recycle Bin',
	'LBL_SELECT_MODULE' => 'Select Module',
	'LBL_EMPTY_RECYCLEBIN' => 'Empty Recycle Bin',
	'LBL_RESTORE' => 'Restore',
	'LBL_NO_PERMITTED_MODULES' => 'No permitted modules available',
	'LBL_RECORDS_LIST' => 'Recycle Bin List',
	'LBL_NO_RECORDS_FOUND' => 'No records found to Restore in module',
);

$jsLanguageStrings = array(
	'JS_MSG_EMPTY_RB_CONFIRMATION' => 'Are you sure you want to permanently remove all the deleted records from your database?',
	'JS_LBL_RESTORE_RECORDS_CONFIRMATION' => 'Are you sure you want to restore the records?',
);


?>