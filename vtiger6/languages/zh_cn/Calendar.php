<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_Calendar' => 'Calendar',
	'LBL_ADD_TASK' => 'Add Task',
	'LBL_ADD_EVENT' => 'Add Event',
	'LBL_RECORDS_LIST' => 'Calendar List',
	'LBL_EVENTS' => 'Events',
	'LBL_TODOS' => 'To Do',
	'LBL_CALENDAR_SETTINGS' => 'Calendar Settings',
	'LBL_CALENDAR_SHARING' => 'Calendar Sharing',
	'LBL_DEFAULT_EVENT_DURATION' => 'Default Event Duration',
	'LBL_CALL' => 'Call',
	'LBL_OTHER_EVENTS' => 'Other Events',
	'LBL_MINUTES' => 'Minutes',
	'LBL_SELECT_USERS' => 'Select Users',

	// Blocks
	'LBL_TASK_INFORMATION' => 'Task Details',

	//Fields
	'Subject' => 'Subject',
	'Start Date & Time' => 'Start Date & Time',
	'Activity Type'=>'Activity Type',
	'Send Notification'=>'Send Notification',
	'Location'=>'Location',
	'End Date & Time' => 'End Date & Time',

	//Side Bar Names
	'LBL_ACTIVITY_TYPES' => 'Activity Types',
	'LBL_CONTACTS_SUPPORT_END_DATE' => 'Support End Date',
	'LBL_CONTACTS_BIRTH_DAY' => 'Date of Birth',
	'LBL_ADDED_CALENDARS' => 'Added Calendars',


	//Activity Type picklist values
	'Call' => 'Call',
	'Meeting' => 'Meeting',
	'Task' => 'Task',

	//Status picklist values
	'Planned' => 'Planned',
	'Completed' => 'Completed',
	'Pending Input' => 'Pending Input',
	'Not Started' => 'Not Started',
	'Deferred' => 'Deferred',

	//Priority picklist values
	'Medium' => 'Medium',

	'LBL_CHANGE_OWNER' => 'Change Owner',

	'LBL_EVENT' => 'Event',
	'LBL_TASK' => 'Task',
	'LBL_TASKS' => 'Tasks',

	'LBL_RECORDS_LIST' => 'List View',
	'LBL_CALENDAR_VIEW' => 'My Calendar',
	'LBL_SHARED_CALENDAR' => 'Shared Calendar',

	//Repeat Lables - used by getTranslatedString
	'LBL_DAY0' => 'Sunday',
	'LBL_DAY1' => 'Monday',
	'LBL_DAY2' => 'Tuesday',
	'LBL_DAY3' => 'Wednesday',
	'LBL_DAY4' => 'Thursday',
	'LBL_DAY5' => 'Friday',
	'LBL_DAY6' => 'Saturday',

	'first' => 'First',
	'last' => 'Last',
	'LBL_DAY_OF_THE_MONTH' => 'day of the month',
	'LBL_ON' => 'on',

	'Daily'=>'Day(s)',
	'Weekly'=>'Week(s)',
	'Monthly'=>'Month(s)',
	'Yearly'=>'Year',

);

$jsLanguageStrings = array(
	'LBL_ADD_EVENT_TASK' => 'Add Event/Task',
	'JS_TASK_IS_SUCCESSFULLY_ADDED_TO_YOUR_CALENDAR' => 'Task is successfully added to your Calendar',
    'LBL_SYNC_BUTTON' => 'Synchronize Now',
    'LBL_SYNCRONIZING' => 'Synchronizing....',
    'LBL_NOT_SYNCRONIZED' => 'You have not synchronized yet',
    'LBL_FIELD_MAPPING' => 'Field Mapping',
    'LBL_CANT_SELECT_CONTACT_FROM_LEADS' => 'Cannot select related Contacts for Leads',
);