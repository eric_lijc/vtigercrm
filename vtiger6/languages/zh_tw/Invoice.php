<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	//DetailView Actions
	'SINGLE_Invoice' => 'Invoice',
	'LBL_EXPORT_TO_PDF' => 'Export to PDF',
    'LBL_SEND_MAIL_PDF' => 'Send Email with PDF',

	//Basic strings
	'LBL_ADD_RECORD' => 'Add Invoice',
	'LBL_RECORDS_LIST' => 'Invoice List',

	// Blocks
	'LBL_INVOICE_INFORMATION' => 'Invoice Details',

	//Field labels
	'Sales Order' => 'Sales Order',
	'Customer No' => 'Customer No',
	'Invoice Date' => 'Invoice Date',
	'Purchase Order' => 'Purchase Order',
	'Sales Commission' => 'Sales Commission',
	'Invoice No' => 'Invoice No',
	'LBL_RECEIVED' => 'Received',
	'LBL_BALANCE' => 'Balance',
	//Added for existing Picklist Entries

	'Sent'=>'Sent',
	'Credit Invoice'=>'Credit Invoice',
	'Paid'=>'Paid',
	'AutoCreated'=>'AutoCreated',
	'Cancel' => 'Cancel',
);
