<?php
/**
 * Copyright (C) 2006-2013 YUCHENG HU
 *
 * ---------------------------------------------------------
 * OSSEZ (中国) 信息技术有限公司
 * http://www.ossez.com
 * http://src.ossez.com
 *
 * CONTACT
 * huyuchengus@gmail.com / yuchenghu@ossez.com
 *
 * ---------------------------------------------------------
 * [A] GNU GENERAL PUBLIC LICENSE GNU/LGPL
 * [B] Apache License, Version 2.0
 *
 * ---------------------------------------------------------
 * NOTE
 * 1. 所有的语言配置文件必须采用无 BOM 的 UTF-8 编码
 * 2. 本语言文件为 ossez-6.0.0 分支，适用于 vTiger 6.0.0
 * ---------------------------------------------------------
 */

$languageStrings = array(
	// Basic Strings
	'SINGLE_ProjectTask' => 'Project Task',
	'LBL_ADD_RECORD' => 'Add Project Task',
	'LBL_RECORDS_LIST' => 'Project Task List',

	// Blocks
	'LBL_PROJECT_TASK_INFORMATION' => 'Project Task Details',
	'LBL_PROJECTS_LIST' => 'Projects List',
	'LBL_TASKS_LIST' => 'Tasks List',
);